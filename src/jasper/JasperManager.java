package jasper;

public interface JasperManager {

	enum Template {
		BEST_SELLERS, SALES_REPORT, TOP_CUSTOMERS
	}

	/**
	 * @return true if and only if the PDF file is successfully created, false
	 *         otherwise.
	 */
	boolean generatePDF(String dataFilePath, Template fileTemplate);

}
