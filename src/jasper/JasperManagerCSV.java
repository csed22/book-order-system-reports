package jasper;

import java.util.Map;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRCsvDataSource;

public class JasperManagerCSV implements JasperManager {

	private String destFile;
	private String jasberTemplate;
	private String[] columnNames;

	@Override
	public boolean generatePDF(String dataFilePath, Template fileTemplate) {

		switch (fileTemplate) {
		case BEST_SELLERS:
			destFile = "best-sellers.pdf";
			jasberTemplate = "resources/best-sellers.jrxml";
			columnNames = new String[] { "ISBN", "Title", "Total Sold Copies" };
			break;
		case SALES_REPORT:
			destFile = "sales-report.pdf";
			jasberTemplate = "resources/sales-report.jrxml";
			columnNames = new String[] { "Sale ID", "Customer Name", "ISBN", "Title", "Qty", "Unit Price", "Price",
					"Date" };
			break;
		case TOP_CUSTOMERS:
			destFile = "top-customers.pdf";
			jasberTemplate = "resources/top-customers.jrxml";
			columnNames = new String[] { "Username", "Last Name", "First Name", "Total Purchase Amount" };
			break;
		}

		try {
			run(dataFilePath);
		} catch (JRException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	// https://github.com/hmkcode/Java/tree/master/java-jasper
	// https://zetcode.com/jasperreports/csv/

	private void run(String srcFile) throws JRException {

		// 1. compile template ".jrxml" file
		JasperReport jasperReport = getJasperReport();

		// 2. parameters "empty"
		Map<String, Object> parameters = getParameters();

		// 3. data source "java object"
		JRDataSource dataSource = getDataSource(srcFile);

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, destFile);

	}

	private JasperReport getJasperReport() throws JRException {
		return JasperCompileManager.compileReport(jasberTemplate);
	}

	private Map<String, Object> getParameters() {
		return new HashMap<>();
	}

	private JRDataSource getDataSource(String srcFile) throws JRException {
		JRCsvDataSource ds = new JRCsvDataSource(srcFile);
		ds.setColumnNames(columnNames);
		return ds;
	}

	public static void main(String[] args) {
		JasperManager jasber = new JasperManagerCSV();
		jasber.generatePDF("resources/top10BestSellers.csv", Template.BEST_SELLERS);
		jasber.generatePDF("resources/totalSalesPrevMonthReport.csv", Template.SALES_REPORT);
		jasber.generatePDF("resources/topFiveCustomersReport.csv", Template.TOP_CUSTOMERS);
	}

}
